var express	= require('express');
var router	= express.Router();
var mysql	= require('mysql');

//DATABASE
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "wortel",
	database: "boggle"
});
con.connect(function(err) {
	if (err)  throw err;
	console.log("Connected!");
});
// DATABASE


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('boggle', { value: 'Boggle API' });
});

router.get('/newboard', function(req, res, next) {

	var dice = [
		['R', 'I', 'F', 'O', 'B', 'X'],
		['I', 'F', 'E', 'H', 'E', 'Y'],
		['D', 'E', 'N', 'O', 'W', 'S'],
		['U', 'T', 'O', 'K', 'N', 'D'],
		['H', 'M', 'S', 'R', 'A', 'O'],
		['L', 'U', 'P', 'E', 'T', 'S'],
		['A', 'C', 'I', 'T', 'O', 'A'],
		['Y', 'L', 'G', 'K', 'U', 'E'],
		['Q', 'B', 'M', 'J', 'O', 'A'],
		['E', 'H', 'I', 'S', 'P', 'N'],
		['V', 'E', 'T', 'I', 'G', 'N'],
		['B', 'A', 'L', 'I', 'Y', 'T'],
		['E', 'Z', 'A', 'V', 'N', 'D'],
		['R', 'A', 'L', 'E', 'S', 'C'],
		['U', 'W', 'I', 'L', 'R', 'G'],
		['P', 'A', 'C', 'E', 'M', 'D']
	];
	var newBoard = '';
	for(var i=0; i<dice.length; i++){
		newBoard += '' + dice[i][ Math.floor( Math.random()*6 ) ];
	}

	res.status(200).json({ board : newBoard });
});

router.get('/highscore', function(req, res, next) {
	var sql = 'SELECT * FROM highscore ORDER BY Score DESC LIMIT 5'
	con.query(sql, function (err, result) {
		if (err) { 
			res.status(500).send(err);
			console.error(err);
		} else {
			res.status(200).json({ highscores : JSON.parse(JSON.stringify(result)) }); //stringify the result, then parse it again for an actual JSON response.
		}
	});

});

router.post('/highscore', function(req, res, next) {
	var sql = 'INSERT INTO highscore (Name, Score, DateTime) VALUES ("' + req.body.Name + '", ' + req.body.Score +', "' + req.body.DateTime + '" )';
	
	con.query(sql, function (err, result) {
		if (err) { 
			res.status(500).send(err);
			//nothing in body? is header: content-type set to application/json ?
			console.error(err);
		} else {
			res.status(204).send();
		}
	});

});

router.get('/checkword', function(req, res, next) {
	var word = req.query.word;
	var isValid = false, score = 0;

	//do checking
	if( req.words.indexOf(word) > -1 ) {
		isValid = true;
	} // words[] contains word
	
	//do scoring
		 if(word.length <= 2) { score = 0; }
	else if(word.length <= 4) { score = 1; }
	else if(word.length == 5) { score = 2; }
	else if(word.length == 6) { score = 3; }
	else if(word.length == 7) { score = 5; }
	else if(word.length >= 8) { score = 11; }
	
	res.status(200).json({ 
			word : word, 
			isValid : isValid, 
			score : (isValid ? score : 0)  // only send score if valid word
	});

})

module.exports = router;
